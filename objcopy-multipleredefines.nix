{
  pkgs ? import <nixpkgs> {}
}:
pkgs.stdenv.mkDerivation rec {
  pname = "objcopy-multipleredefines";
  version = "2.38";

  patchTree = builtins.fetchGit {
    url = "https://gitlab.com/urskogshumla/patches.git";
  };

  src = builtins.fetchurl {
    url = "https://ftp.gnu.org/gnu/binutils/binutils-2.38.tar.gz";
  };

  buildInputs = [
  ];

  configurePhase = ''
    (cd bfd && ./configure) \
    && (cd libiberty && ./configure) \
    && (cd zlib && ./configure) \
    && (cd binutils && ./configure && patch < ${patchTree.outPath + "/updated_gotos_apr2022.patch"})
  '';

  buildPhase = ''
    (cd bfd && make) \
    && (cd libiberty && make) \
    && (cd zlib && make) \
    && (cd binutils && make objcopy)
  '';

  installPhase = ''
    mkdir -p $out/bin
    mv binutils/objcopy $out/bin/objcopy-multipleredefines
  '';
}

