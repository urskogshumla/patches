diff --git a/binutils/objcopy.c b/binutils/objcopy.c
index 28b9d3bf92..08c1674eb6 100644
--- a/binutils/objcopy.c
+++ b/binutils/objcopy.c
@@ -54,11 +54,28 @@ struct is_specified_symbol_predicate_data
   bfd_boolean	found;
 };
 
+#define REDEFINE_FLAGS_COUNT (2)
+enum redefine_mode_flags
+{
+  REDEFINE_FLAGS_DEFINED   = 1 << 0,
+  REDEFINE_FLAGS_UNDEFINED = 1 << 1,
+  REDEFINE_FLAGS_BOTH     = REDEFINE_FLAGS_DEFINED | REDEFINE_FLAGS_UNDEFINED
+};
+enum redefine_mode
+{
+  REDEFINE_DEFINED   = REDEFINE_FLAGS_DEFINED,
+  REDEFINE_UNDEFINED = ((REDEFINE_DEFINED << REDEFINE_FLAGS_COUNT)
+			| REDEFINE_FLAGS_UNDEFINED),
+  REDEFINE_BOTH      = ((REDEFINE_UNDEFINED << REDEFINE_FLAGS_COUNT)
+			| REDEFINE_FLAGS_BOTH)
+};
+
 /* A node includes symbol name mapping to support redefine_sym.  */
 struct redefine_node
 {
   char *source;
   char *target;
+  enum redefine_mode mode;
 };
 
 struct addsym_node
@@ -339,7 +356,11 @@ enum command_line_switch
   OPTION_PURE,
   OPTION_READONLY_TEXT,
   OPTION_REDEFINE_SYM,
+  OPTION_REDEFINE_DEFINED_SYM,
+  OPTION_REDEFINE_UNDEFINED_SYM,
   OPTION_REDEFINE_SYMS,
+  OPTION_REDEFINE_DEFINED_SYMS,
+  OPTION_REDEFINE_UNDEFINED_SYMS,
   OPTION_REMOVE_LEADING_CHAR,
   OPTION_REMOVE_RELOCS,
   OPTION_RENAME_SECTION,
@@ -470,7 +491,11 @@ static struct option copy_options[] =
   {"pure", no_argument, 0, OPTION_PURE},
   {"readonly-text", no_argument, 0, OPTION_READONLY_TEXT},
   {"redefine-sym", required_argument, 0, OPTION_REDEFINE_SYM},
+  {"redefine-defined-sym", required_argument, 0, OPTION_REDEFINE_DEFINED_SYM},
+  {"redefine-undefined-sym", required_argument, 0, OPTION_REDEFINE_UNDEFINED_SYM},
   {"redefine-syms", required_argument, 0, OPTION_REDEFINE_SYMS},
+  {"redefine-defined-syms", required_argument, 0, OPTION_REDEFINE_DEFINED_SYMS},
+  {"redefine-undefined-syms", required_argument, 0, OPTION_REDEFINE_UNDEFINED_SYMS},
   {"remove-leading-char", no_argument, 0, OPTION_REMOVE_LEADING_CHAR},
   {"remove-section", required_argument, 0, 'R'},
   {"remove-relocations", required_argument, 0, OPTION_REMOVE_RELOCS},
@@ -535,7 +560,7 @@ static void get_sections (bfd *, asection *, void *);
 static int compare_section_lma (const void *, const void *);
 static void mark_symbols_used_in_relocations (bfd *, asection *, void *);
 static bfd_boolean write_debugging_info (bfd *, void *, long *, asymbol ***);
-static const char *lookup_sym_redefinition (const char *);
+static const char *lookup_sym_redefinition (const char *, const enum redefine_mode_flags);
 static const char *find_section_rename (const char *, flagword *);
 
 ATTRIBUTE_NORETURN static void
@@ -1508,9 +1533,11 @@ filter_symbols (bfd *abfd, bfd *obfd, asymbol **osyms,
 
       if (htab_elements (redefine_specific_htab) || section_rename_list)
 	{
+	  enum redefine_mode_flags mode_flags;
 	  char *new_name;
 
-	  new_name = (char *) lookup_sym_redefinition (name);
+	  mode_flags = undefined ? REDEFINE_FLAGS_UNDEFINED : REDEFINE_FLAGS_DEFINED;
+	  new_name = (char *) lookup_sym_redefinition (name, mode_flags);
 	  if (new_name == name
 	      && (flags & BSF_SECTION_SYM) != 0)
 	    new_name = (char *) find_section_rename (name, NULL);
@@ -1690,26 +1717,29 @@ filter_symbols (bfd *abfd, bfd *obfd, asymbol **osyms,
 /* Find the redefined name of symbol SOURCE.  */
 
 static const char *
-lookup_sym_redefinition (const char *source)
+lookup_sym_redefinition (const char *source, const enum redefine_mode_flags flags)
 {
-  struct redefine_node key_node = {(char *) source, NULL};
+  struct redefine_node key_node = {(char *) source, NULL, REDEFINE_BOTH};
   struct redefine_node *redef_node
     = (struct redefine_node *) htab_find (redefine_specific_htab, &key_node);
 
-  return redef_node == NULL ? source : redef_node->target;
+  return ((redef_node != NULL && redef_node->mode & flags)
+	  ? redef_node->target
+	  : source);
 }
 
 /* Insert a node into symbol redefine hash tabel.  */
 
 static void
 add_redefine_and_check (const char *cause, const char *source,
-			const char *target)
+			const char *target, const enum redefine_mode mode)
 {
   struct redefine_node *new_node
     = (struct redefine_node *) xmalloc (sizeof (struct redefine_node));
 
   new_node->source = strdup (source);
   new_node->target = strdup (target);
+  new_node->mode   = mode;
 
   if (htab_find (redefine_specific_htab, new_node) != HTAB_EMPTY_ENTRY)
     fatal (_("%s: Multiple redefinition of symbol \"%s\""),
@@ -1732,7 +1762,7 @@ add_redefine_and_check (const char *cause, const char *source,
    from the file, and add them to the symbol redefine list.  */
 
 static void
-add_redefine_syms_file (const char *filename)
+add_redefine_syms_file (const char *filename, const enum redefine_mode mode)
 {
   FILE *file;
   char *buf;
@@ -1810,7 +1840,7 @@ add_redefine_syms_file (const char *filename)
 	end_of_line:
 	  /* Append the redefinition to the list.  */
 	  if (buf[0] != '\0')
-	    add_redefine_and_check (filename, &buf[0], &buf[outsym_off]);
+	    add_redefine_and_check (filename, &buf[0], &buf[outsym_off], mode);
 
 	  lineno++;
 	  len = 0;
@@ -4805,6 +4835,7 @@ copy_main (int argc, char *argv[])
   int c;
   struct stat statbuf;
   const bfd_arch_info_type *input_arch = NULL;
+  enum redefine_mode redefine_mode = REDEFINE_BOTH;
 
   while ((c = getopt_long (argc, argv, "b:B:i:I:j:K:MN:s:O:d:F:L:G:R:SpgxXHhVvW:wDU",
 			   copy_options, (int *) 0)) != EOF)
@@ -5208,6 +5239,12 @@ copy_main (int argc, char *argv[])
 	  remove_leading_char = TRUE;
 	  break;
 
+	case OPTION_REDEFINE_DEFINED_SYM:
+	  redefine_mode >>= REDEFINE_FLAGS_COUNT;
+	  __attribute__((fallthrough));
+	case OPTION_REDEFINE_UNDEFINED_SYM:
+	  redefine_mode >>= REDEFINE_FLAGS_COUNT;
+	  __attribute__((fallthrough));
 	case OPTION_REDEFINE_SYM:
 	  {
 	    /* Insert this redefinition onto redefine_specific_htab.  */
@@ -5231,15 +5268,25 @@ copy_main (int argc, char *argv[])
 	    target = (char *) xmalloc (len + 1);
 	    strcpy (target, nextarg);
 
-	    add_redefine_and_check ("--redefine-sym", source, target);
+	    add_redefine_and_check ("--redefine-sym", source, target, redefine_mode);
 
+	    redefine_mode = REDEFINE_BOTH;
 	    free (source);
 	    free (target);
 	  }
 	  break;
 
+	case OPTION_REDEFINE_DEFINED_SYMS:
+	  redefine_mode >>= REDEFINE_FLAGS_COUNT;
+	  __attribute__((fallthrough));
+	case OPTION_REDEFINE_UNDEFINED_SYMS:
+	  redefine_mode >>= REDEFINE_FLAGS_COUNT;
+	  __attribute__((fallthrough));
 	case OPTION_REDEFINE_SYMS:
-	  add_redefine_syms_file (optarg);
+	  {
+	    add_redefine_syms_file (optarg, redefine_mode);
+	    redefine_mode = REDEFINE_BOTH;
+	  }
 	  break;
 
 	case OPTION_SET_SECTION_FLAGS:
